var gulp = require('gulp');
var uncss = require('gulp-uncss');
var gutil = require('gulp-util');


var paths = {
    css: ['static/styles/**/*.css'],
    javascript: ['static/scripts/**/*.js'],
    js: ['static/scripts/**/*.js'],
    html: ['layouts/**/*.html']
};



gulp.task('min', function() {
    return gulp.src(['static/styles/main.css'])
        .pipe(uncss({
            html:paths.html
        }))
        .pipe(gulp.dest('src/dist'));
});


gulp.task('hello', function() {
    gutil.log(paths.css);

});

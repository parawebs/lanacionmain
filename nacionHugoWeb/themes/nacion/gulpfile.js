var gulp = require('gulp');
var uncss = require('gulp-uncss');
var gutil = require('gulp-util');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');


var paths = {
    css: ['static/styles/**/*.css'],
    css2: ['static/styles/vendor.css'],
    javascript: ['static/scripts/**/*.js'],
    js: ['static/scripts/**/*.js'],
    html: ['layouts/**/*.html', 'http://betalanacion.parawebs.com/', 'http://betalanacion.parawebs.com/sucesos', 'http://betalanacion.parawebs.com/sucesos/mariangel-ruiz-y-su-hija-fueron-victimas-de-un-intento-de-asalto/'],
    jsmain: ['static/scripts/sugar-1.4.1.js', 'static/scripts/modernizr.js', 'static/scripts/riot+compiler.min.js', 'static/scripts/jquery-2.1.4.min.js', 'static/scripts/jquery.bxslider.min.js', 'static/scripts/jquery.onscreen.min.js', 'static/scripts/jquery.sidr.min.js', 'static/scripts/jquery.touchSwipe.min.js', 'static/scripts/bootstrap.min.js', 'static/scripts/bootstrap-hover-dropdown.min.js', 'static/scripts/response.min.js', 'static/scripts/main.js']

};


var nano = require('gulp-cssnano');

gulp.task('css', function() {
    return gulp.src(paths.css)
        .pipe(concat('dln.css'))
        .pipe(nano())
        .pipe(gulp.dest('src/dist/css'));
});


gulp.task('min', function() {
    return gulp.src(paths.css2)
        .pipe(uncss({
            html: paths.html
        }))
        .pipe(gulp.dest('src/dist/js'));
});


gulp.task('hello', function() {
    gutil.log(paths.css);

});

gulp.task('compress', function() {
    return gulp.src(paths.jsmain)
        .pipe(concat('dln.js'))
        .pipe(uglify())
        .pipe(gulp.dest('src/dist/js'));
});

#!/usr/bin/env python3
import threading
import requests
import json
from json import dumps, load
import datetime
import locale
import re
import urllib.request
import urllib.parse
import time
import yaml
##############################
from textblob import TextBlob
import shutil
from yaml import load, dump
import unicodedata
from subprocess import call
import os
import sys
from unidecode import unidecode
import html
import pprint
from bs4 import BeautifulSoup
import os.path

####################
import glob
import PIL
from PIL import Image, ImageOps
import sys

##############
import hashlib
from parse_rest.connection import register
from parse_rest.datatypes import Object
##################### Utilidades Made #########################
#from imgutils import thumb



debug = True

currentDir = os.path.dirname(os.path.abspath(__file__))

locale.setlocale(locale.LC_ALL, 'es_VE.utf8')

APPLICATION_ID = "mxPOm008hyVfunzWFCKB98kEiMHnwkFrMNiOoS4n"
REST_API_KEY = "uJLctPaJxsFXWTUEUz3ia2Bi6wWaUvGnCjTns59N"
MASTER_KEY = "d45xcY8ZH962yU7zVMhAT9IsQIHxi54zUvosSwaz"
register(APPLICATION_ID, REST_API_KEY)

def thumb(path, type=None):
    if path:
        try:
            photo = path.split("/")[-1]
            newPhoto = urllib.parse.quote(photo)
            path = path.replace(photo, newPhoto)
            ext = path.split(".")[-1]
            m = hashlib.md5()
            m.update(photo.encode('utf-8'))
            filename = str(m.hexdigest()) + "." + ext
            exist = os.path.isfile(currentDir + "/temp/" + filename)
            if not exist:
                print(path)
                save_path = currentDir + "/temp/" + filename
                headers = {'User-Agent': 'Mozilla/5.0'}
                req = urllib.request.Request(path, headers=headers)
                with urllib.request.urlopen(req) as response, open(
                        save_path, 'wb') as out_file:
                    data = response.read()  # a `bytes` object
                    out_file.write(data)

                img_path = currentDir + "/temp/" + filename
                modified_path = currentDir + '/fotoedicion/thumb_' + filename
                if type == None:
                    size = (600, 400)
                    crop_type = 'top'
                    # If height is higher we resize vertically, if not we resize
                    # horizontally
                    img = Image.open(img_path)
                    # Get current and desired ratio for the images
                    img_ratio = img.size[0] / float(img.size[1])
                    ratio = size[0] / float(size[1])
                    # The image is scaled/cropped
                    # vertically or horizontally depending on
                    # the ratio
                    if ratio > img_ratio:
                        img = img.resize((size[0], int(round(size[0] * img.size[1] / img.size[0]))),
                                         Image.ANTIALIAS)
                        # Crop in the top, middle or bottom
                        if crop_type == 'top':
                            box = (0, 0, img.size[0], size[1])
                        elif crop_type == 'middle':
                            box = (0, int(round((img.size[1] - size[1]) / 2)), img.size[0],
                                   int(round((img.size[1] + size[1]) / 2)))
                        elif crop_type == 'bottom':
                            box = (
                                0, img.size[1] - size[1], img.size[0], img.size[1])
                        else:
                            raise ValueError(
                                'ERROR: invalid value for crop_type')
                        img = img.crop(box)
                    elif ratio < img_ratio:
                        img = img.resize((int(round(size[1] * img.size[0] / img.size[1])), size[1]),
                                         Image.ANTIALIAS)
                        # Crop in the top, middle or bottom
                        if crop_type == 'top':
                            box = (0, 0, size[0], img.size[1])
                        elif crop_type == 'middle':
                            box = (int(round((img.size[0] - size[0]) / 2)), 0,
                                   int(round((img.size[0] + size[0]) / 2)), img.size[1])
                        elif crop_type == 'bottom':
                            box = (
                                img.size[0] - size[0], 0, img.size[0], img.size[1])
                        else:
                            raise ValueError(
                                'ERROR: invalid value for crop_type')
                        img = img.crop(box)
                    else:
                        img = img.resize((size[0], size[1]),
                                         Image.ANTIALIAS)
                    # If the scale is the same, we do not need to crop
                    img.save(modified_path)
                    print(filename)
                    return filename
                elif type == "obituarios":
                    basewidth = 500
                    img = Image.open(img_path)
                    wpercent = (basewidth / float(img.size[0]))
                    hsize = int((float(img.size[1]) * float(wpercent)))
                    img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
                    img.save(modified_path)
                    return filename
            else:
                return filename
        except:
            raise
            return "84c20ac6c9a47486381aceb7f6ccb466.jpg"
    else:
        return "84c20ac6c9a47486381aceb7f6ccb466.jpg"

def parsesave(post):
    news = Object.factory("Noticias")
    post = news(title=post['title'], image=post['featured'], slug=post[
                'slug'], idN=post['id'], category=post['cat'],reads=0)
    post.save()


def gettags(html):
    soup = BeautifulSoup(html)
    text = soup.getText()
    blob = TextBlob(text)

    count = []
    for tags in blob.tags:
        if (tags[1] == 'NNP'):
            if (len(tags[0]) > 3):
                count.append(str(tags[0]))
    unique = []
    i = 0
    for item in count:
        if item not in unique:
            i = i + 1
            if i > 3:
                break
            unique.append(item)
    return unique





def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

def genGallery(gallery):
    gallery = re.search(r'(ids\=\").*\"', gallery)
    gallery = gallery.group(0)
    gallery = gallery.replace("ids=", "")
    gallery = gallery.replace('"', "")
    fotos = gallery.split(",")
    lista = ""
    for foto in fotos:
        getObituarioUrl(foto)
        lista = lista + '<li><img src="/fotoedicion/thumb_' + \
            str(thumb(getObituarioUrl(foto), "obituarios")) + \
            '" style="max-width: 350px;"></li>'

    content = '<ul style=" list-style-type: none;" class="text-center">' + \
        lista + "</ul>"
    print(content)



def parsepost(post):
    soup = BeautifulSoup(post)
    try:
        h = soup.find("div", {"class": "wp-caption"})
        h.extract()
    except:
        pass
    try:
        h = soup.findAll("img")
        h[0].extract()
    except:
        pass
    try:
        h = soup.findAll("a")
        h[0].extract()
    except:
        pass
    post = str(soup)
    post = post.replace("<html>", "").replace("<body>", "").replace("</body>", "").replace("</html>", "")
    post = re.sub(r'\[/?caption[^\]]*?\]', '', post)

    gallery = re.search(r'\[gallery.*\]', post)
    if gallery:
        gallery = gallery.group(0)
        genGallery(gallery)


    return str(post)


def getObituarioUrl(post):
    soup = BeautifulSoup(post)
    try:
        h = soup.find("div", {"class": "wp-caption"})
        h.extract()
    except:
        pass
    try:
        h = soup.findAll("img")
        h[0].extract()
    except:
        pass
    try:
        h = soup.findAll("a")
        h[0].extract()
    except:
        pass

    try:
        post = str(soup)
        post = post.replace("<html>", "").replace(
            "<body>", "").replace("</body>", "").replace("</html>", "")
        #post = post.replace("<body>", "")
        post = re.sub(r'\[/?caption[^\]]*?\]', '', post)
        post = re.search(r'(ids\=\").*\"', post)
        post = post.group(0)
        post = post.replace("ids=", "")
        post = post.replace('"', "")
        fotos = post.split(",")
        lista = ""
        for foto in fotos:
            print(foto)
            getObituarioUrl(foto)
            lista = lista + '<li><img src="/fotoedicion/thumb_' + \
                str(thumb(getObituarioUrl(foto), "obituarios")) + \
                '" style="max-width: 350px;"></li>'

        content = '<ul style=" list-style-type: none;" class="text-center">' + \
            lista + "</ul>"
    except:
        if debug:
            raise
        else:    
            pass
        content=""

    return str(content)


def parsejson(file, mode):
    title = file['title']
    title = html.unescape(title)
    id = file['id']
    categories = file['category'][0]['name'].lower()
    categoslug = file['category'][0]['slug'].lower()
    cats = [file['category'][0]['slug'].lower()]    
    try:
        if (file['tags'][0]['slug'] == "portada"):
            onhomepage = "yes"
        else:
            onhomepage = "no"
    except:
        onhomepage = "no"

    css = unidecode(categories).replace(" ", "")
    if css == "obituarios":
        print("obituariosParse")
        content = obituarios(file['content'])
    else:
        content = parsepost(file['content'])

    published = datetime.datetime.strptime(
        file['date_gmt'][0:16], '%Y-%m-%d %H:%M').strftime('%A %d de %B, %Y | %I:%M %p')

    try:
        excerpt = cleanhtml(content)
        excerpt = (excerpt[:100] + '..') if len(excerpt) > 100 else excerpt
    except:
        excerpt = ""

    try:
        featured = file['featured_image']['guid']
    except:
        featured = None

    slug = file['slug']
    slug2 = categoslug + "/" + file['slug']

    date = datetime.datetime.strptime(
        file['date_gmt'], '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%dT%H:%M:%S')

    

    try:
        tags = gettags(content)
        filename = thumb(featured)
        post = yaml.dump({'categories': cats, 'date': date, 'tags': tags, 
            'title': title, 'id': id, 'category': categories,
            'published': published, 'excerpt': excerpt, 'css': css,
            'featured': filename, 'slug': slug2,'portada' : onhomepage})

        exist = os.path.isfile(currentDir + "/content/" + slug + ".md")
        if not exist:
            parsesave({'title': title, 'featured': filename,
                       'slug': slug2, 'id': id, 'cat': categoslug})
            print(title)
            f = open(currentDir + "/content/" + slug + ".md", 'w')
            f.write('---\n')
            f.write(post)
            f.write('---\n\n')
            if content:
                f.write(content)
            f.close()
        if mode == "featured":
            post = {'title': title, 'category': categories,
                    'css': css, 'featured': filename, 'slug': slug2}
            return post
    except:
        if debug:
            raise
        else:    
            pass


def getObituarioUrl(id):
    r = requests.get(
        'http://www.lanacionweb.com/parawebs.php?password=0987123&img=' + id)
    resp = r.json()
    return resp[0]['guid']


def featured():
    r = requests.get('http://www.lanacionweb.com/parawebs.php?password=0987123&action=featured')
    feadturedpost = []
    for post in r.json():
        feadturedpost.append(parsejson(post, "featured"))

    with open(currentDir + "/dynamic/featured.json", 'w') as outfile:
        json.dump(feadturedpost, outfile)
'''
#http://www.lanacionweb.com/parawebs.php?password=0987123&action=featured
r = requests.get('http://www.lanacionweb.com/parawebs.php?password=0987123&post=425764')
for post in r.json():
    parsejson(post, None)

#featured()
'''
'''


#r = requests.get('http://127.0.0.1/1.json')
# parsejson(r.json(),None)
#featured()
'''


with open("post.json") as json_file:
    json_data = json.load(json_file)

for post in json_data:
    parsejson(post, None)


exit()


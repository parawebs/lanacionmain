import urllib
import glob
import sys
import hashlib
import os
import unicodedata
import locale
import glob
import PIL
from PIL import Image, ImageOps
import sys
import urllib


currentDir = os.path.dirname(os.path.abspath(__file__))
locale.setlocale(locale.LC_ALL, 'es_VE.utf8')

def thumb(path, type=None):
    if path:
        try:
            photo = path.split("/")[-1]
            newPhoto = urllib.parse.quote(photo)
            path = path.replace(photo, newPhoto)
            ext = path.split(".")[-1]
            m = hashlib.md5()
            m.update(photo.encode('utf-8'))
            filename = str(m.hexdigest()) + "." + ext
            exist = os.path.isfile(currentDir + "/temp/" + filename)
            if not exist:
                print(path)
                save_path = currentDir + "/temp/" + filename
                headers = {'User-Agent': 'Mozilla/5.0'}
                req = urllib.request.Request(path, headers=headers)
                with urllib.request.urlopen(req) as response, open(
                        save_path, 'wb') as out_file:
                    data = response.read()  # a `bytes` object
                    out_file.write(data)

                img_path = currentDir + "/temp/" + filename
                modified_path = currentDir + '/fotoedicion/thumb_' + filename
                if type == None:
                    size = (600, 400)
                    crop_type = 'top'
                    # If height is higher we resize vertically, if not we resize
                    # horizontally
                    img = Image.open(img_path)
                    # Get current and desired ratio for the images
                    img_ratio = img.size[0] / float(img.size[1])
                    ratio = size[0] / float(size[1])
                    # The image is scaled/cropped
                    # vertically or horizontally depending on
                    # the ratio
                    if ratio > img_ratio:
                        img = img.resize((size[0], int(round(size[0] * img.size[1] / img.size[0]))),
                                         Image.ANTIALIAS)
                        # Crop in the top, middle or bottom
                        if crop_type == 'top':
                            box = (0, 0, img.size[0], size[1])
                        elif crop_type == 'middle':
                            box = (0, int(round((img.size[1] - size[1]) / 2)), img.size[0],
                                   int(round((img.size[1] + size[1]) / 2)))
                        elif crop_type == 'bottom':
                            box = (
                                0, img.size[1] - size[1], img.size[0], img.size[1])
                        else:
                            raise ValueError(
                                'ERROR: invalid value for crop_type')
                        img = img.crop(box)
                    elif ratio < img_ratio:
                        img = img.resize((int(round(size[1] * img.size[0] / img.size[1])), size[1]),
                                         Image.ANTIALIAS)
                        # Crop in the top, middle or bottom
                        if crop_type == 'top':
                            box = (0, 0, size[0], img.size[1])
                        elif crop_type == 'middle':
                            box = (int(round((img.size[0] - size[0]) / 2)), 0,
                                   int(round((img.size[0] + size[0]) / 2)), img.size[1])
                        elif crop_type == 'bottom':
                            box = (
                                img.size[0] - size[0], 0, img.size[0], img.size[1])
                        else:
                            raise ValueError(
                                'ERROR: invalid value for crop_type')
                        img = img.crop(box)
                    else:
                        img = img.resize((size[0], size[1]),
                                         Image.ANTIALIAS)
                    # If the scale is the same, we do not need to crop
                    img.save(modified_path)
                    print(filename)
                    return filename
                elif type == "obituarios":
                    basewidth = 500
                    img = Image.open(img_path)
                    wpercent = (basewidth / float(img.size[0]))
                    hsize = int((float(img.size[1]) * float(wpercent)))
                    img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
                    img.save(modified_path)
                    return filename
            else:
                return filename
        except:
            raise
            return "84c20ac6c9a47486381aceb7f6ccb466.jpg"
    else:
        return "84c20ac6c9a47486381aceb7f6ccb466.jpg"
import re
import json
from bs4 import BeautifulSoup
import requests
from imgutils import thumb
from imageprocess import imageprocess

#####Post Parse #####
import html
from unidecode import unidecode
import datetime
from shortener import BitlyShortener
from aylienapiclient import textapi
import yaml
import os
from parse_rest.connection import register
from parse_rest.datatypes import Object
from htmlcleaner import cleanse_html
import sys

from bottle import route, run,request
import threading
import subprocess



class Postparser():
    def __init__(self, **kwargs):
        if not kwargs.get('debug', False):
            self.debug = False
        self.debug = kwargs.get('debug')
        self.shortener = BitlyShortener()
        self.textanalyzer = textapi.Client("fc8b92ee", "87b9aa44c51f78e07462c28fb3a6e2c2")
        self.currentDir = os.path.dirname(os.path.abspath(__file__))
        #self.featured()
        self.APPLICATION_ID = "mxPOm008hyVfunzWFCKB98kEiMHnwkFrMNiOoS4n"
        self.REST_API_KEY = "uJLctPaJxsFXWTUEUz3ia2Bi6wWaUvGnCjTns59N"
        self.MASTER_KEY = "d45xcY8ZH962yU7zVMhAT9IsQIHxi54zUvosSwaz"

    def clean(self, raw_html):
        cleanr = re.compile('<.*?>')
        cleantext = re.sub(cleanr, '', raw_html)
        return cleantext

    def parseDB(self, post):
        print("saving...")
        register("mxPOm008hyVfunzWFCKB98kEiMHnwkFrMNiOoS4n", "uJLctPaJxsFXWTUEUz3ia2Bi6wWaUvGnCjTns59N")
        news = Object.factory("Noticias")
        post = news(title=post['title'], image=post['featured'], slug=post[
            'slug'], idN=post['id'], category=post['cat'], reads=0, text=post['text'],date=post['date'],
                    mobiledate=post['mobiledate'],excerpt=post['excerpt'],published=post['published'])
        post.save()

    def featured(self):
        r = requests.get('http://www.lanacionweb.com/parawebs.php?password=0987123&action=featured')
        feadturedpost = []
        for post in r.json():
            feadturedpost.append(self.posts(post, "featured"))

        with open(self.currentDir + "/dynamic/featured.json", 'w') as outfile:
            json.dump(feadturedpost, outfile)

    def photoInfo(self, id, type):
        r = requests.get(
            'http://www.lanacionweb.com/parawebs.php?password=0987123&img=' + id)
        resp = r.json()
        img = imageprocess(resp[0]['guid'], type)
        print({"img": img, "caption": resp[0]['post_excerpt']})
        return ({"img": img, "caption": resp[0]['post_excerpt']})

    def galleryGen(self, gallery):
        gallery = gallery.group(0)
        gallery = re.search(r'ids\=\".*\"', gallery)
        gallery = gallery.group(0)

        gallery = gallery.replace("ids=", "")
        gallery = gallery.replace('"', "")
        fotos = gallery.split(",")
        lista = []
        lista.append('<div class="row text-center">')
        for foto in fotos:
            data = self.photoInfo(foto, None)
            # lista.append()
            item = '<a href="/fotoedicion/thumb_{image}" data-toggle="lightbox" data-title="{title}" data-footer="{' \
                   'caption}"><img ' \
                   'src="/fotoedicion/thumb_{image}" style="max-width: 200px;padding-left: 5px;padding-right: ' \
                   '5px;padding-top: ' \
                   '5px;padding-bottom: 5px;" class="col-md-4 col-lg-4 col-sm-6 col-xs-6"></a>'.format(
                image=data['img'],
                caption=data[
                    'caption'], title=self.title)
            lista.append(item)
        lista.append('</div>')
        # print("".join(lista))
        return "".join(lista)

    def figure(self, tag):
        print("hola")

        img = re.search(r'src\s*=\s*"(.+?)"', str(tag.group(0)))
        caption = re.search(r'\/>(.*?)\[', str(tag.group(1)))
        if caption is None:
            caption = ""
        else:
            caption = caption.group(1)

        if img is None:
            return ""
        else:
            img = img.group(1)
            img = imageprocess(img, None)
            item = '<div style="max-width: 600px;max-height: 402px;"><img src="/fotoedicion/thumb_{image}"><div class="fotoleyenda">{' \
                   'caption}</div></div>'.format(image=img,
                                                 caption=caption)
            return item

    def content(self, post):
        soup = BeautifulSoup(post)

        h = soup.find("div", {"class": "wp-caption"})
        if h:
            h.extract()

        h = soup.findAll("img")
        if h:
            h[0].extract()

        h = soup.findAll("a")
        if h:
            h[0].extract()

        post = str(soup)
        post = re.sub(r'(<(?!\/)[^>]+>)+(<\/[^>]+>)+', '', post)
        # post = re.sub(r'<([a-z0-9]+)>(<br\s*/>|\&nbsp;|\&#160;|\s)*</\1>', '', post)
        post = re.sub(r'\n(\s*\n)+', '</p><p>', post)

        post = post.replace("<html>", "").replace(
            "<body>", "").replace("</body>", "").replace("</html>", "")
        pattern = '<figure([\w\W]+?)</figure>'
        post = re.sub(pattern, self.figure, post)
        post = re.sub(r'\[caption.*\]', '', post)
        gallery = re.search(r'\[gallery.*\]', post)
        if gallery:
            # gallery = gallery.group(0)
            post = re.sub('\[gallery.*\]', self.galleryGen, post)

        return post

    def tagsGen(self, text, title):
        hastags = self.textanalyzer.Hashtags({"text": text, "language": "es"})

        to_remove = [i for i, val in enumerate(hastags['hashtags']) if len(val)> 12]

        for index in reversed(to_remove):
            del hastags['hashtags'][index]
        tags = []
        cantidad = len(hastags['hashtags'])
        for i in range(3 if cantidad > 3 else cantidad):
            tags.append(hastags['hashtags'][i])
        return tags

    def posts(self, file, mode):

        title = html.unescape(file['title'])
        self.title = title
        id = file['id']
        print(id)
        categories = file['category'][0]['name'].lower()
        # slug para el permalink
        categoslug = file['category'][0]['slug'].lower()
        cats = [file['category'][0]['slug'].lower()]
        # Saber si va a la portada.
        try:
            onindex = 'yes' if file['tags'][0]['slug'] == "portada"  else 'no'
        except IndexError:
            onindex = 'no'

        # parse la categoria para ponerle el css correspondiente
        css = unidecode(categories).replace(" ", "")
        if css == "obituarios":
            print("Obituarios Parse")
            # content = obituarios(file['content'])
        else:
            print("Content Parser")
            content = self.content(file['content'])

        published = datetime.datetime.strptime(
            file['date_gmt'][0:16], '%Y-%m-%d %H:%M').strftime('%d %B, %Y | %I:%M %p')

        mobiledate = datetime.datetime.strptime(
            file['date_gmt'][0:16], '%Y-%m-%d %H:%M').strftime('%d de %B, %Y')
        excerpt = self.clean(content)
        excerpt = (excerpt[:100] + '...') if len(excerpt) > 100 else excerpt

        featured = file['featured_image']['guid']
        featured = featured if featured else None

        slug = file['slug']
        slug2 = categoslug + "/" + file['slug']

        date = ((datetime.datetime.strptime(
            file['date_gmt'], '%Y-%m-%d %H:%M:%S')) - datetime.timedelta(minutes=270)).strftime('%Y-%m-%dT%H:%M:%S')
        tags = self.tagsGen(content, title)

        filename = thumb(featured)
        post = yaml.dump({'categories': cats, 'date': date, 'tags': tags,
                          'title': title, 'id': id, 'category': categories,
                          'published': published, 'excerpt': excerpt, 'css': css,
                          'featured': filename, 'slug': slug2, 'portada': onindex,'mobiledate':mobiledate})

        self.parseDB({'title': title, 'featured': filename,
                      'slug': slug2, 'id': id, 'cat': categoslug, 'text': content,'excerpt':excerpt,'date': date,
                      'mobiledate':mobiledate,'published':published})
        f = open(self.currentDir + "/content/" + slug + ".md", 'w')
        f.write('---\n')
        f.write(post)
        f.write('---\n\n')
        if content:
            f.write(content)
        f.close()
        if mode == "featured":
            post = {'title': title, 'category': categories,
                    'css': css, 'featured': filename, 'slug': slug2}
            return

        subprocess.Popen(['sh','gen.sh'], stdout=subprocess.PIPE)




'''
with open("post.json") as json_file:
    json_data = json.load(json_file)

for post in json_data:
    parser.posts(post, None)


try:
    post = str(sys.argv[1])
    if len(post) > 10:
        url = post
    else:

except IndexError:
    uri = "http://www.lanacionweb.com/parawebs.php?password=0987123&action=last"




'''
parser = Postparser(debug=False)
parser.downloadImg = thumb

def getPost(post):
    uri = "http://www.lanacionweb.com/parawebs.php?password=0987123&post="+post
    r = requests.get(uri)
    for post in r.json():
        parser.posts(post, None)


def hello(post):
    p = threading.Thread(target=getPost, args=(post,))
    p.daemon = True
    p.start()
    #p.join()
    f = threading.Thread(target=)
    f.daemon = True
    f.start()


    return {'status':'queue'}

run(host='0.0.0.0', port=8080, debug=False)
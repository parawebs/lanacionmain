from urllib.request import urlopen
from wand.image import Image
import hashlib
import locale
import os
import urllib

currentDir = os.path.dirname(os.path.abspath(__file__))
locale.setlocale(locale.LC_ALL, 'es_VE.utf8')


def imageprocess(url,type):
    photo = url.split("/")[-1]
    newPhoto = urllib.parse.quote(photo)
    path = url.replace(photo, newPhoto)
    ext = url.split(".")[-1]
    m = hashlib.md5()
    m.update(photo.encode('utf-8'))
    filename = str(m.hexdigest()) + "." + ext
    exist = os.path.isfile(currentDir + "/fotoedicion/thumb_" + filename)
    if not exist:
        headers = {'User-Agent': 'Mozilla/5.0'}
        req = urllib.request.Request(url, headers=headers)
        file = urllib.request.urlopen(req)
        image = Image(file=file)
        size = (600, 400)
        img_ratio = image.width / float(image.height)
        ratio = size[0] / float(size[1])
        if ratio > img_ratio:
            print(">")
            with image.clone() as resize:
                if (type=="obituarios"):
                    resize.transform(resize='500')
                else:
                    resize.resize(600,400)

                resize.save(filename=currentDir + "/fotoedicion/thumb_" + filename)
                resize.size
                #img.crop(width=600, height=400, gravity='center')
                #img.save(filename='seam-resize.jpg')
        else:
            print("else")
            with image.clone() as resize:
                if (type=="obituarios"):
                    resize.transform(resize='500')
                else:
                    resize.resize(600,400)

                resize.save(filename=currentDir + "/fotoedicion/thumb_" + filename)
                resize.size
                #img.crop(width=600, height=400, gravity='center')
                #img.save(filename='seam-resize.jpg')

    return filename


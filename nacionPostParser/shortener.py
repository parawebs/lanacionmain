__author__ = 'gerswin'
import requests
import json
class BitlyShortener():
    api_url = 'https://api-ssl.bit.ly/'

    def __init__(self):

        self.login = "lanacionve"
        self.api_key = "R_0412b3835a2840fab3a5bb6bd7b1db5c"
        self.token = "d419ecb26993cd8574dee6ba2a27a8066d16278b"

    def short(self, url):
        shorten_url = '{}{}'.format(self.api_url, 'v3/shorten')
        params = dict(
            uri=url,
            x_apiKey=self.api_key,
            x_login=self.login,
            access_token=self.token,
        )
        response = requests.post(shorten_url, data=params)
        if response.ok:
            data = response.json()
            if 'status_code' in data and data['status_code'] == 200:
                return data['data']['url']

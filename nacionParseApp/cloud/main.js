Parse.Cloud.define("mostRead", function(request, response) {
    var Noticias = Parse.Object.extend("Noticias");
    var query = new Parse.Query(Noticias);
    query.equalTo("idN", parseInt(request.params.news));
    query.first().then(function(news) {
        console.log(news)
        news.increment("reads");
        return news.save();
    }).then(function(news) {
        response.success("ok");
    });
});
Parse.Cloud.define("relatedNews", function(request, response) {
    var Noticias = Parse.Object.extend("Noticias");
    var query = new Parse.Query(Noticias);
    var skipvar = Math.floor(Math.random() * (3 - 0)) + 1;
    query.equalTo("category", request.params.category);
    query.limit(3)
    query.skip(skipvar)
    query.notEqualTo("idN", parseInt(request.params.news));
    query.find().then(function(news) {
        if (news.length == 3) {
            response.success(news);
        } else {
            query.limit(3)
            query.skip(0)
            return query.find();
        }
    }).then(function(news) {
        if (typeof news !== 'undefined') {
            response.success(news);

        }

    });
});

Parse.Cloud.define("recomendNews", function(request, response) {
    var Noticias = Parse.Object.extend("Noticias");
    var query = new Parse.Query(Noticias);
    var skipvar = Math.floor(Math.random() * (3 - 0)) + 1;
    query.equalTo("category", request.params.category);
    query.limit(3)
    query.skip(skipvar)
    query.notEqualTo("idN", parseInt(request.params.news));
    query.find().then(function(news) {
        if (news.length == 3) {
            response.success(news);
        } else {
            query.limit(3)
            query.skip(0)
            return query.find();
        }
    }).then(function(news) {
        if (typeof news !== 'undefined') {
            response.success(news);

        }

    });
});

Parse.Cloud.define("topNews", function(request, response) {
    var d = new Date()
    var today = new Date(d.getTime() - d.getTimezoneOffset() * 60000).toJSON().slice(0, 10)
    var Noticias = Parse.Object.extend("Noticias");
    var query = new Parse.Query(Noticias);
    query.limit(4)
    query.descending('reads')

    query.find().then(function(news) {
        response.success(news);

    });
});

var _ = require("underscore");
Parse.Cloud.beforeSave("Noticias", function(request, response) {
    var text = request.object;

    var toLowerCase = function(w) {
        return w.toLowerCase();
    };

    regex = /(<([^>]+)>)/ig;
    words = text.get("text").replace(regex, "");
    words = words.split(" ");
    title = text.get("title").split(" ");

    console.log(words);
    words = _.map(words, toLowerCase);
    var stopWords = ["a", "actualmente", "acuerdo", "adelante", "ademas", "además", "adrede", "afirmó", "agregó", "ahi", "ahora", "ahí", "al", "algo", "alguna", "algunas", "alguno", "algunos", "algún", "alli", "allí", "alrededor", "ambos", "ampleamos", "antano", "antaño", "ante", "anterior", "antes", "apenas", "aproximadamente", "aquel", "aquella", "aquellas", "aquello", "aquellos", "aqui", "aquél", "aquélla", "aquéllas", "aquéllos", "aquí", "arriba", "arribaabajo", "aseguró", "asi", "así", "atras", "aun", "aunque", "ayer", "añadió", "aún", "b", "bajo", "bastante", "bien", "breve", "buen", "buena", "buenas", "bueno", "buenos", "c", "cada", "casi", "cerca", "cierta", "ciertas", "cierto", "ciertos", "cinco", "claro", "comentó", "como", "con", "conmigo", "conocer", "conseguimos", "conseguir", "considera", "consideró", "consigo", "consigue", "consiguen", "consigues", "contigo", "contra", "cosas", "creo", "cual", "cuales", "cualquier", "cuando", "cuanta", "cuantas", "cuanto", "cuantos", "cuatro", "cuenta", "cuál", "cuáles", "cuándo", "cuánta", "cuántas", "cuánto", "cuántos", "cómo", "d", "da", "dado", "dan", "dar", "de", "debajo", "debe", "deben", "debido", "decir", "dejó", "del", "delante", "demasiado", "demás", "dentro", "deprisa", "desde", "despacio", "despues", "después", "detras", "detrás", "dia", "dias", "dice", "dicen", "dicho", "dieron", "diferente", "diferentes", "dijeron", "dijo", "dio", "donde", "dos", "durante", "día", "días", "dónde", "e", "ejemplo", "el", "ella", "ellas", "ello", "ellos", "embargo", "empleais", "emplean", "emplear", "empleas", "empleo", "en", "encima", "encuentra", "enfrente", "enseguida", "entonces", "entre", "era", "eramos", "eran", "eras", "eres", "es", "esa", "esas", "ese", "eso", "esos", "esta", "estaba", "estaban", "estado", "estados", "estais", "estamos", "estan", "estar", "estará", "estas", "este", "esto", "estos", "estoy", "estuvo", "está", "están", "ex", "excepto", "existe", "existen", "explicó", "expresó", "f", "fin", "final", "fue", "fuera", "fueron", "fui", "fuimos", "g", "general", "gran", "grandes", "gueno", "h", "ha", "haber", "habia", "habla", "hablan", "habrá", "había", "habían", "hace", "haceis", "hacemos", "hacen", "hacer", "hacerlo", "haces", "hacia", "haciendo", "hago", "han", "hasta", "hay", "haya", "he", "hecho", "hemos", "hicieron", "hizo", "horas", "hoy", "hubo", "i", "igual", "incluso", "indicó", "informo", "informó", "intenta", "intentais", "intentamos", "intentan", "intentar", "intentas", "intento", "ir", "j", "junto", "k", "l", "la", "lado", "largo", "las", "le", "lejos", "les", "llegó", "lleva", "llevar", "lo", "los", "luego", "lugar", "m", "mal", "manera", "manifestó", "mas", "mayor", "me", "mediante", "medio", "mejor", "mencionó", "menos", "menudo", "mi", "mia", "mias", "mientras", "mio", "mios", "mis", "misma", "mismas", "mismo", "mismos", "modo", "momento", "mucha", "muchas", "mucho", "muchos", "muy", "más", "mí", "mía", "mías", "mío", "míos", "n", "nada", "nadie", "ni", "ninguna", "ningunas", "ninguno", "ningunos", "ningún", "no", "nos", "nosotras", "nosotros", "nuestra", "nuestras", "nuestro", "nuestros", "nueva", "nuevas", "nuevo", "nuevos", "nunca", "o", "ocho", "os", "otra", "otras", "otro", "otros", "p", "pais", "para", "parece", "parte", "partir", "pasada", "pasado", "paìs", "peor", "pero", "pesar", "poca", "pocas", "poco", "pocos", "podeis", "podemos", "poder", "podria", "podriais", "podriamos", "podrian", "podrias", "podrá", "podrán", "podría", "podrían", "poner", "por", "porque", "posible", "primer", "primera", "primero", "primeros", "principalmente", "pronto", "propia", "propias", "propio", "propios", "proximo", "próximo", "próximos", "pudo", "pueda", "puede", "pueden", "puedo", "pues", "q", "qeu", "que", "quedó", "queremos", "quien", "quienes", "quiere", "quiza", "quizas", "quizá", "quizás", "quién", "quiénes", "qué", "r", "raras", "realizado", "realizar", "realizó", "repente", "respecto", "s", "sabe", "sabeis", "sabemos", "saben", "saber", "sabes", "salvo", "se", "sea", "sean", "segun", "segunda", "segundo", "según", "seis", "ser", "sera", "será", "serán", "sería", "señaló", "si", "sido", "siempre", "siendo", "siete", "sigue", "siguiente", "sin", "sino", "sobre", "sois", "sola", "solamente", "solas", "solo", "solos", "somos", "son", "soy", "soyos", "su", "supuesto", "sus", "suya", "suyas", "suyo", "sé", "sí", "sólo", "t", "tal", "tambien", "también", "tampoco", "tan", "tanto", "tarde", "te", "temprano", "tendrá", "tendrán", "teneis", "tenemos", "tener", "tenga", "tengo", "tenido", "tenía", "tercera", "ti", "tiempo", "tiene", "tienen", "toda", "todas", "todavia", "todavía", "todo", "todos", "total", "trabaja", "trabajais", "trabajamos", "trabajan", "trabajar", "trabajas", "trabajo", "tras", "trata", "través", "tres", "tu", "tus", "tuvo", "tuya", "tuyas", "tuyo", "tuyos", "tú", "u", "ultimo", "un", "una", "unas", "uno", "unos", "usa", "usais", "usamos", "usan", "usar", "usas", "uso", "usted", "ustedes", "v", "va", "vais", "valor", "vamos", "van", "varias", "varios", "vaya", "veces", "ver", "verdad", "verdadera", "verdadero", "vez", "vosotras", "vosotros", "voy", "vuestra", "vuestras", "vuestro", "vuestros", "w", "x", "y", "ya", "yo", "z", "él", "ésa", "ésas", "ése", "ésos", "ésta", "éstas", "éste", "éstos", "última", "últimas", "último", "últimos"];
    words = _.filter(words, function(w) {
        return (w.length > 1) && !_.contains(stopWords, w);
    });
    title = _.filter(title, function(w) {
        return (w.length > 1) && !_.contains(stopWords, w);
    });
    text.set("words", words);
    text.set("titletags", title);
    //post.set("hashtags", hashtags);
    response.success();
});

Parse.Cloud.afterSave("Noticias", function(request) {

    var Noticias = Parse.Object.extend("Noticias");
    var query = new Parse.Query(Noticias);
    query.equalTo("idN", request.object.get("idN"));

    // Order them by creation in db (oldest in DB first)
    query.descending("createdAt");

    query.find({

        success: function(results) {

            if (results && results.length > 1) {
                // We have a least one duplicate

                // Destroy every Profile except for the original (the oldest in DB)
                for (var i = (results.length - 1); i > 0; i--) {
                    results[i].destroy();
                    // /!\ In case of concurrent call of afterSave, 
                    // we might run in situation of concurrent deletetion 
                    // of same Profile in this case we might encounter (or not?) some 
                    // time out of this function buuut we will always keep 
                    // the same Profile from being deleted.

                }
            } else {
                // No duplicates
            }

        },
        error: function(error) {}
    });
});
var express = require('express');
var app = express();

app.use(express.bodyParser());
app.get('/top', function(req, res) {
    var Noticias = Parse.Object.extend("Noticias");
    var query = new Parse.Query(Noticias);
    query.limit(4)
    query.descending('reads')
    query.find().then(function(news) {
        res.header("Access-Control-Allow-Origin", "*");
        res.json(news);

    });

});

app.listen();
